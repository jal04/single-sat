FROM python:3.6 AS single-sat

RUN apt-get update \
    && apt-get -y install git \
    && mkdir ./WORKDIR/ \
# Configure apt and install packages
RUN apt-get update \
    && apt-get -y install python3-tk \
    && apt-get -y install python3-pip \
    && apt-get -y install minisat \
    && pip3 install pandas \
    && pip3 install numpy \